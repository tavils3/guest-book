<form method="post" action="/" class="form">
  <div>
    <input type="text" placeholder="Ваше имя" name="name" class="form__field"
      value="<?php echo isset($_COOKIE['name']) ? $_COOKIE['name'] : '' ?>"
    />
    <div class="form__field-error"><?php echo isset($nameError) ? $nameError : ''; ?></div>
  </div>
  <div>
    <input type="email" placeholder="Ваш email" name="email" class="form__field"
      value="<?php echo isset($_COOKIE['email']) ? $_COOKIE['email'] : '' ?>" 
    />
    <div class="form__field-error"><?php echo isset($emailError) ? $emailError : ''; ?></div>
  </div>
  <div>
    <textarea name="message" placeholder="Ваше сообщение" 
      class="form__field form__field_textarea"><?php echo isset($_COOKIE['message']) ? $_COOKIE['message'] : '' ?></textarea>
    <div class="form__field-error"><?php echo isset($messageError) ? $messageError : ''; ?></div>
  </div>
  <div><input type="submit" name="submit" value="Отправить сообщение" /></div>
  <div class="form__success"><?php echo isset($resultMessage) ? $resultMessage : ''; ?></div>
</form>