<form method="post" action="/">
<ul class="messages-list">
<?php
  while($row = $messagesSource->fetch_assoc()) {
?>
  <li class="messages-list__item">
    <div class="messages-list__item-text"><?php echo $row['message']; ?></div>
    <div class="messages-list__item-name"><?php echo $row['name']; ?></div>
    <button value="<?php echo $row['id']; ?>" type="submit" name="delete" class="messages-list__item-button">Удалить</button>
  </li>
<?php
  }
?>
</ul>
</form>
<div class="pagination"><?php echo $pagination; ?></div>