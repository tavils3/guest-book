<?php

class MessagesDB {
  
  static $mysqli;
  static $messagesPerPage = 5;
  
  static public function init() {
    MessagesDB::$mysqli = new mysqli('localhost', 'root', '', 'guest_book');
  }
  
  static public function close() {
    MessagesDB::$mysqli->close();
  }
  
  static public function insert($name, $email, $message) {
    $name = MessagesDB::$mysqli->real_escape_string($name);
    $email = MessagesDB::$mysqli->real_escape_string($email);
    $message = MessagesDB::$mysqli->real_escape_string($message);
    $result = MessagesDB::$mysqli->query("INSERT INTO messages VALUES (NULL, '$name', '$email', '$message')");
    if ($result) {
      return true;
    }
    return false;
  }
  
  static public function select() {
    if (MessagesDB::getCurPage() == 1) {
      $offset = 0;
    } else {
      $offset = (MessagesDB::getCurPage() - 1) * MessagesDB::$messagesPerPage;
    }
    $limit = MessagesDB::$messagesPerPage;
    $result = MessagesDB::$mysqli->query("SELECT * FROM messages ORDER BY id DESC LIMIT $offset, $limit");
    return $result;
  }
  
  static public function getCurPage() {
    if (!isset($_GET['page'])) {
      return 1;
    } else {
      return $_GET['page'];
    }
  }
  
  static public function pagination() {
    $curPage = MessagesDB::getCurPage();
    $result = MessagesDB::$mysqli->query("SELECT COUNT(*) as count FROM messages");
    $messagesCount = $result->fetch_assoc()['count'];
    $pagesCount = ceil($messagesCount / MessagesDB::$messagesPerPage);
    $pagination = '';
    if ($pagesCount == 1) {
      return $pagination;
    }
    for ($i = 1; $i <= $pagesCount; ++$i) {
      if ($curPage == $i) {
        $pagination .= '<span class="pagination__item pagination__item_active">'.$i.'</span>';
      } else {
        $url = 'http://'.$_SERVER['SERVER_NAME'].'?page='.$i;
        $pagination .= '<a href="'.$url.'" class="pagination__item">'.$i.'</a>';
      }
    }
    return $pagination;
  }
  
  static public function delete($value) {
    $result = MessagesDB::$mysqli->query("DELETE FROM messages WHERE id = $value");
    if ($result) {
      return true;
    }
    return false;
  }
}

class View {
  
  static public function getTemplate($tmpName, $tmpData = array()) {
    extract($tmpData);
    ob_start();
    include 'templates/' . $tmpName . '.php';
    echo ob_get_clean();
  }
  
}

MessagesDB::init();
View::getTemplate('header');

if (isset($_POST['submit'])) {
  $tmpData = array(
    'nameError' => '',
    'emailError' => '',
    'messageError' => '',
    'resultMessage' => ''
  );
  $formValid = true;
  $name = $_POST['name'] ? trim($_POST['name']) : '';
  $email = $_POST['email'] ? trim($_POST['email']) : '';
  $message = $_POST['message'] ? trim($_POST['message']) : '';
  $_COOKIE['name'] = $name;
  $_COOKIE['email'] = $email;
  $_COOKIE['message'] = $message;
  if ($name === '') {
    $tmpData['nameError'] = 'Поле обязательно для заполнения';
    $formValid = false;
  } else if (strlen($name) > 100) {
    $tmpData['nameError'] = 'Длина превышает 100 символов';
    $formValid = false;
  }
  if ($email === '') {
    $tmpData['emailError'] = 'Поле обязательно для заполнения';
    $formValid = false;
  } else if (strlen($email) > 100) {
    $tmpData['emailError'] = 'Длина превышает 100 символов';
    $formValid = false;
  }
  if ($message === '') {
    $tmpData['messageError'] = 'Поле обязательно для заполнения';
    $formValid = false;
  } else if (strlen($message) > 20000) {
    $tmpData['messageError'] = 'Длина превышает 20000 символов';
    $formValid = false;
  }
  if ($formValid) {
    if (MessagesDB::insert($name, $email, $message)) {
      $_COOKIE['name'] = '';
      $_COOKIE['email'] = '';
      $_COOKIE['message'] = '';
      $tmpData['resultMessage'] = 'Сообщение добавлено';
    } else {
      $tmpData['resultMessage'] = 'Ошибка добавления сообщения';
    }
  }
  View::getTemplate('form', $tmpData);
} else if (isset($_POST['delete'])) {
  $tmpData = array('resultMessage' => '');
  if (MessagesDB::delete($_POST['delete'])) {
    $tmpData['resultMessage'] = 'Сообщение удалено';
  } else {
    $tmpData['resultMessage'] = 'Не удалось удалить сообщение';
  }
  View::getTemplate('form', $tmpData);
} else {
  View::getTemplate('form');
}

$messagesSource = MessagesDB::select();
$pagination = MessagesDB::pagination();

View::getTemplate('messages', array('messagesSource' => $messagesSource, 'pagination' => $pagination));

View::getTemplate('footer');
MessagesDB::close();

?>